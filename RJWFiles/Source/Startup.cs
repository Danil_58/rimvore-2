﻿using System.Reflection;
using HarmonyLib;
using Verse;

namespace RV2_RJW
{
    [StaticConstructorOnStartup]
    public static class Startup
    {
        static Startup()
        {
            Harmony.DEBUG = false;
            Harmony harmony = new Harmony("rv2_rjw");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }
}
