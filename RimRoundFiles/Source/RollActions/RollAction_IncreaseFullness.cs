﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimRound;
using RimVore2;
using RimRound.Comps;
using RimRound.Patch;
using RimWorld;

namespace RV2_RimRound
{
    public class RollAction_IncreaseFullness : RollAction
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            if (!base.TryAction(record, rollStrength))
                return false;

            FullnessAndDietStats_ThingComp fullnessComp = TargetPawn.TryGetComp<FullnessAndDietStats_ThingComp>();
            if (fullnessComp == null)
            {
                FullnessFallback(record, rollStrength);
                return false;
            }

            Thing_Ingested_HarmonyPatch.Postfix(OtherPawn, TargetPawn, ref rollStrength);
            RV2Log.Message($"Increased {TargetPawn.LabelShort} fullness by {rollStrength} for a new total of {fullnessComp.CurrentFullness}", true, false, "RimRound");
            return true;
        }

        private void FullnessFallback(VoreTrackerRecord record, float rollStrength)
        {
            RV2Log.Message($"Pawn {TargetPawn.LabelShort} has no fullness comp, falling back to normal food need increase", true, false, "RimRound");
            Need_Food foodNeed = TargetPawn.needs?.TryGetNeed<Need_Food>();
            if (foodNeed == null)
            {
                RV2Log.Message($"Pawn {TargetPawn.LabelShort} has no food need either, doing nothing", true, false, "RimRound");
                return;
            }
            foodNeed.CurLevel += rollStrength;
        }
    }
}
