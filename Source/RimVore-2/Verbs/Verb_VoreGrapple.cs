﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.AI;

namespace RimVore2
{
    public class Verb_VoreGrapple : Verb_MeleeAttack
    {
        protected override DamageWorker.DamageResult ApplyMeleeDamageToTarget(LocalTargetInfo target)
        {
            Job grappleJob = JobMaker.MakeJob(VoreJobDefOf.RV2_VoreGrapple, target);
            grappleJob.expiryInterval = int.MaxValue;
            CasterPawn.jobs.StartJob(grappleJob, JobCondition.Succeeded);
            return new DamageWorker.DamageResult();
        }

        public override bool Available()
        {
            if(!base.Available())
                return false;
            if(!RV2Mod.Settings.features.GrapplingEnabled)
                return false;
            return base.CasterPawn.CanParticipateInVore(out _);
        }

        public override bool IsUsableOn(Thing target)
        {
            if(!base.IsUsableOn(target))
            {
                RV2Log.Message($"{CasterPawn.LabelShort} - Grapple not usable, base check returned false", true, false, "VoreCombatGrapple");
                return false;
            }
            if(!(target is Pawn targetPawn))
            {
                RV2Log.Message($"{CasterPawn.LabelShort} - Grapple not usable, target is not a pawn, target is {(target == null ? "NULL" : target.GetType().ToString())}", true, false, "VoreCombatGrapple");
                return false;
            }
            if(CombatUtility.IsInvolvedInGrapple(targetPawn) || CombatUtility.IsInvolvedInGrapple(base.CasterPawn))
            {
                RV2Log.Message($"{CasterPawn.LabelShort} - Grapple not usable, either pawn is already involved in a grapple", true, false, "VoreCombatGrapple");
                return false;
            }
            // the pawn must be able to receive the "grappled" hediff
            if(targetPawn.health == null)
            {
                RV2Log.Message($"{CasterPawn.LabelShort} - Grapple not usable, target has no health", true, false, "VoreCombatGrapple");
                return false;
            }
            float grappleStrength = CombatUtility.GetGrappleStrength(base.CasterPawn, true);
            if(grappleStrength <= 0)
            {
                RV2Log.Message($"{CasterPawn.LabelShort} - Grapple not usable, grapple strength is too low: {grappleStrength}", true, false, "VoreCombatGrapple");
                return false;
            }
            if(!base.CasterPawn.CanVore(targetPawn, out _))
            {
                RV2Log.Message($"{CasterPawn.LabelShort} - Grapple not usable, can't vore target", true, false, "VoreCombatGrapple");
                return false;
            }
            bool useAutoRules = RV2Mod.Settings.combat.UseAutoRules;
            VoreInteractionRequest request = new VoreInteractionRequest(CasterPawn, targetPawn, VoreRole.Predator, useAutoRules);
            VoreInteraction interaction = VoreInteractionManager.Retrieve(request);
            if(interaction.PreferredPath == null)
            {
                RV2Log.Message($"{CasterPawn.LabelShort} - Grapple not usable, no preferred path", true, false, "VoreCombatGrapple");
                return false;
            }

            return true;
        }

        public override void OrderForceTarget(LocalTargetInfo target)
        {
            Job job = JobMaker.MakeJob(VoreJobDefOf.RV2_VoreGrapple, target);
            job.playerForced = true;
            base.CasterPawn.jobs.TryTakeOrderedJob(job);
        }
    }
}
