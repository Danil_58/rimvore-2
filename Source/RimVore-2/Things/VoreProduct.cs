﻿using System;
using System.Collections.Generic;
using Verse;

namespace RimVore2
{
    /// <summary>
    /// Re-pack digested remains and/or add new items
    /// </summary>
    /// <remarks>
    /// Following possibilities: 
    /// * items set, container and containerItems not set: The normal contents of the VoreContainer are excreted alongside any Thing items provided
    /// * items, container set and containerItems not set: The normal contents of the VoreContainer are moved the container item, which is excreted alongside other items (like spawned filth)
    /// * items, container and containerItems set: The normal contents of the VoreContainer are moved to the container item along with whatever items were provided in containerItems. The container alongside the provided items will be excreted
    /// </remarks>
    public class VoreProduct
    {
        public bool dessicatePrey = true;
        public bool destroyPrey = false;

        public List<ThingDef> items;
        public List<ThingDef> containerItems;
        //public ContainerSelectorDef containerSelector;
        public List<ThingDef> selectableContainers;

        public List<ThingDef> ValidContainers => selectableContainers.FindAll(container => container.GetModExtension<VoreContainerExtension>().IsValid());

        //public List<Thing> MakeVoreProducts(ThingOwner previousContainer, VorePathDef vorePathDef, Pawn predator)
        //{
        //    if(items == null && ValidContainers.NullOrEmpty())
        //    {
        //        return new List<Thing>();
        //    }
        //    List<Thing> producedItems = new List<Thing>();
        //    if (items != null)
        //    {
        //        producedItems.AddRange(items.ConvertAll(thingDef => ThingMaker.MakeThing(thingDef)));
        //        RV2Log.Message("producedItems: " + string.Join(", ", producedItems.ConvertAll(t => t.ToString())), "OngoingVore");
        //    }
        //    if(!ValidContainers.NullOrEmpty())
        //    {
        //        VoreProductContainer producedContainer = CreateProductContainer(vorePathDef, predator);
        //        if (producedContainer == null)
        //        {
        //            RV2Log.Warning("The container was null, skipping");
        //            return producedItems;
        //        }
        //        RV2Log.Message("producedContainer: " + producedContainer.def.defName, true, false, "OngoingVore");
        //        bool containerHasProducedItems = producedContainer.def != RV2_Common.VoreContainerNone;
        //        if (!containerHasProducedItems)
        //        {
        //            return producedItems;
        //        }
        //        List<Thing> producedContainerItems = containerItems?.ConvertAll(thingDef => ThingMaker.MakeThing(thingDef));
        //        if(producedContainerItems != null)
        //        {
        //            RV2Log.Message("producedContainerItems: " + string.Join(", ", producedContainerItems.ConvertAll(t => t.ToString())), true, false, "OngoingVore");
        //            producedContainer.GetDirectlyHeldThings().TryAddRangeOrTransfer(producedContainerItems);
        //        }
        //        previousContainer.TryTransferAllToContainer(producedContainer.GetDirectlyHeldThings());
        //        producedItems.Add(producedContainer);
        //    }
        //    return producedItems;
        //}

        public void AddVoreProducts(VoreTrackerRecord record)
        {
            if(record.VoreContainer == null)
            {
                RV2Log.Error("No VoreContainer for record " + record.ToString());
                return;
            }
            VoreProductContainer productContainer = record.VoreContainer.VoreProductContainer;
            if(productContainer != null)
            {
                // move all items in the non-product container into the product container (this moves the corpse into the product container)
                //RV2Log.Message("Transfering all items from VoreContainer to VoreProductContainer", "VoreContainer");
                //record.VoreContainer.GetDirectlyHeldThings().TryTransferAllToContainer(productContainer.GetDirectlyHeldThings());
                // then move the VoreProductContainer INTO the VoreContainer
                //record.VoreContainer.TryAddItem(productContainer);
                // produce additional items for the product container
                if(!containerItems.NullOrEmpty())
                {
                    RV2Log.Message("Creating container items", "VoreContainer");
                    List<Thing> producedContainerItems = containerItems?.ConvertAll(thingDef => ThingMaker.MakeThing(thingDef));
                    if(!producedContainerItems.NullOrEmpty())
                    {
                        RV2Log.Message("producedContainerItems: " + string.Join(", ", producedContainerItems.ConvertAll(t => t.ToString())), true, false, "VoreContainer");
                        productContainer.GetDirectlyHeldThings().TryAddRangeOrTransfer(producedContainerItems);
                    }
                }
            }
            else
            {
                RV2Log.Message("No VoreProductContainer!");
            }
            // produce items that are excreted *alongside* the product container
            if(!items.NullOrEmpty())
            {
                RV2Log.Message("Creating items", "VoreContainer");
                List<Thing> producedItems = containerItems?.ConvertAll(thingDef => ThingMaker.MakeThing(thingDef));
                if(!producedItems.NullOrEmpty())
                {
                    RV2Log.Message("producedItems: " + string.Join(", ", producedItems.ConvertAll(t => t.ToString())), true, false, "VoreContainer");
                    record.VoreContainer.TryAddOrTransfer(producedItems);
                }
            }
        }

        public VoreProductContainer CreateProductContainer(VorePathDef vorePathDef, Pawn predator, Pawn prey = null)
        {
            if(ValidContainers.NullOrEmpty())
            {
                return null;
            }
            ThingDef containerDef = RV2Mod.Settings.rules.GetContainer(predator, vorePathDef);
            if(containerDef == null)
            {
                return null;
            }
            VoreProductContainer productContainer = (VoreProductContainer)ThingMaker.MakeThing(containerDef);
            productContainer.Initialize(prey);
            return productContainer;
        }
    }
}
