﻿#if !v1_2

using System;
using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;

namespace RimVore2
{
    [HarmonyPatch(typeof(PawnAttackGizmoUtility), "GetAttackGizmos")]
    public class Patch_PawnAttackGizmoUtility
    {
        [HarmonyPostfix]
        private static IEnumerable<Gizmo> AddGrappleGizmo(IEnumerable<Gizmo> __result, Pawn pawn)
        {
            foreach(Gizmo gizmo in __result)
            {
                yield return gizmo;
            }
            if(pawn == null)   // some user had a null pawn here, I am absolutely baffled, but okay
                yield break;
            try
            {
                bool canGrapple = CombatUtility.GetGrappleChance(pawn, true) > 0;
                if(!canGrapple)
                    yield break;
                if(!GrappleGizmoAvailable(pawn, out _))
                    yield break;
            }
            catch(Exception e)
            {
                Log.Warning("RimVore-2: Something went wrong when trying to add the grapple gizmo: " + e);
                yield break;
            }
            // bit odd, but you can not yield return in a try-catch, so the real gizmo yield happens down here
            yield return GetGrappleAttackGizmo(pawn);
        }

        private static Gizmo GetGrappleAttackGizmo(Pawn pawn)
        {
            Command_Target command = new Command_Target()
            {
                defaultLabel = "RV2_Command_Grapple".Translate(),
                defaultDesc = "RV2_Command_Grapple_Desc".Translate(),
                targetingParams = GrappleTragetingParameters(pawn),
                icon = UITextures.GrappleAttacker
            };

            if(!CombatUtility.CanGrapple(pawn, out string reason))
            {
                command.Disable(reason);
            }
            command.action = (LocalTargetInfo target) =>
            {
                Pawn targetPawn = target.Pawn;
                Action grappleAction = GrappleAction(pawn, targetPawn, out reason);
                if(grappleAction != null)
                    grappleAction();
                else
                    Messages.Message(reason, targetPawn, MessageTypeDefOf.RejectInput, false);
            };
            return command;

        }
        private static bool GrappleGizmoAvailable(Pawn pawn, out string reason)
        {
            if(!pawn.Drafted)
            {
                reason = "IsNotDraftedLower".Translate(pawn.LabelShort, pawn);
                return false;
            }
            if(!pawn.IsColonistPlayerControlled)
            {
                reason = "CannotOrderNonControlledLower".Translate();
                return false;
            }
            if(pawn.WorkTagIsDisabled(WorkTags.Violent))
            {
                reason = "IsIncapableOfViolenceLower".Translate(pawn.LabelShort, pawn);
                return false;
            }

            reason = null;
            return true;
        }
        private static Action GrappleAction(Pawn pawn, LocalTargetInfo target, out string failureReason)
        {
            if(!GrappleGizmoAvailable(pawn, out failureReason))
            {
                return null;
            }
            if(target.IsValid && !pawn.CanReach(target, PathEndMode.Touch, Danger.Deadly, false, false, TraverseMode.ByPawn))
            {
                failureReason = "NoPath".Translate();
                return null;
            }
            Pawn targetPawn = target.Pawn;
            if(pawn == targetPawn)
            {
                failureReason = "CannotAttackSelf".Translate();
                return null;
            }
            bool inSameFaction = pawn.Faction == targetPawn.Faction
                || pawn.InSameExtraFaction(targetPawn, ExtraFactionType.HomeFaction)
                || pawn.InSameExtraFaction(targetPawn, ExtraFactionType.MiniFaction);
            if(inSameFaction)
            {
                failureReason = "CannotAttackSameFactionMember".Translate();
                return null;
            }

            failureReason = null;
            return () =>
            {
                Job gotoJob = JobMaker.MakeJob(JobDefOf.Goto, targetPawn);
                pawn.jobs.TryTakeOrderedJob(gotoJob, JobTag.DraftedOrder);

                Job grappleJob = JobMaker.MakeJob(VoreJobDefOf.RV2_VoreGrapple, targetPawn);
                pawn.jobs.jobQueue.EnqueueFirst(grappleJob, JobTag.DraftedOrder);
            };
        }
        private static TargetingParameters GrappleTragetingParameters(Pawn pawn)
        {
            Predicate<TargetInfo> validator = (TargetInfo target) =>
            {
                Pawn targetPawn = target.Thing as Pawn;
                if(targetPawn == null)
                    return false;
                if(!CombatUtility.CanGrapple(pawn, out _, targetPawn))
                    return false;
                return true;
            };
            return new TargetingParameters()
            {
                canTargetBuildings = false,
                validator = validator
            };
        }
    }
}
#endif