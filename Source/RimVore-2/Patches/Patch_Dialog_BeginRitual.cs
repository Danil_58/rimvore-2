﻿#if !v1_2
using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    [HarmonyPatch(typeof(Dialog_BeginRitual), "BlockingIssues")]
    public static class Patch_Dialog_BeginRitual
    {
        /// <summary>
        /// This was originally a pass-through enumeration patch, but it produces a NRE for base game rituals
        /// I have no idea why that in particular malfunctions, but I will blame Harmony for it
        /// </summary>
        /// <param name="__result"></param>
        /// <param name="___ritual"></param>
        /// <param name="___assignments"></param>
        [HarmonyPostfix]
        private static void AddVoreValidatorIssues(ref IEnumerable<string> __result, Precept_Ritual ___ritual, RitualRoleAssignments ___assignments)
        {
            try
            {
                List<string> issues = __result != null ? __result.ToList() : new List<string>();
                VoreRitualValidator validator = ___ritual?.behavior?.def?.GetModExtension<VoreRitualValidator>();
                if(validator == null)
                {
                    RV2Log.Message("no validator for " + ___ritual?.def?.defName, true, true, "Rituals");
                    return;
                }
                //RV2Log.Message("roles: " + String.Join(", ", ___assignments.AllRolesForReading.Select(r => r.id)), false, true);
                //foreach(var x in ___assignments.AllRolesForReading)
                //{
                //    RV2Log.Message("assigned to " + x.Label + ": " + String.Join(", ", ___assignments.AssignedPawns(x)), false, true);
                //}
                if(!validator.IsValid(___assignments, out string reason))
                {
                    issues.Add(reason);
                }
                __result = issues.AsEnumerable();
            }
            catch(Exception e)
            {
                RV2Log.Error("Something went wrong when gathering vore ritual issues: " + e, false, true);
            }
        }
    }
}
#endif