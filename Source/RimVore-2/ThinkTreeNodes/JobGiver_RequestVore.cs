﻿/// No longer directly used from the ThinKTree since Vore became a work type and proposals are generated in WorkGiver_ProposeVore

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using RimWorld;
using Verse.AI.Group;

namespace RimVore2
{
    public class JobGiver_RequestVore : ThinkNode_JobGiver
    {
        protected virtual IEnumerable<Pawn> GetValidPawns(Pawn pawn)
        {
            Map map = pawn.Map;
            if(map == null)
            {
                RV2Log.Message("Early exit: no map determinable", true, false, "AutoVore");
                return null;
            }
            List<Pawn> validTargets = map.mapPawns.FreeColonistsSpawned;
            validTargets.AddDistinctRange(map.mapPawns.PrisonersOfColonySpawned);
            if(RV2Mod.Settings.features.AnimalsEnabled)
            {
#if v1_2
                IEnumerable<Pawn> spawnedColonyAnimals = map.mapPawns.AllPawnsSpawned
                .Where(p =>
                    p.RaceProps?.Animal == true
                    && p.Faction == Faction.OfPlayer
                );
                validTargets.AddDistinctRange(spawnedColonyAnimals);
#else
                // colony animal list is only tracked in 1.3+ 
                validTargets.AddDistinctRange(map.mapPawns.SpawnedColonyAnimals);
#endif
            }
            validTargets = validTargets
                .Where(t => ValidTargetFor(t, pawn))
                .ToList();    // check if vore is even possible between the pawns

            // remove self from targets
            validTargets.Remove(pawn);
            return validTargets;
        }

        public static bool ValidTargetFor(Pawn potentialTarget, Pawn initiator)
        {
            return !potentialTarget.InAggroMentalState    // do not propose to hostile pawns
                && initiator.CanReserveAndReach(potentialTarget, PathEndMode.Touch, Danger.Some)    // check pathing and reservation ability
                && (initiator.CanVore(potentialTarget, out _) || potentialTarget.CanVore(initiator, out _))
                && RV2Mod.Settings.rules.CanBeProposedTo(potentialTarget);
        }

        protected override Job TryGiveJob(Pawn pawn)
        {
            RV2Log.Message("Tried to give request-vore job!", true, false, "AutoVore");
            IEnumerable<Pawn> validTargets = GetValidPawns(pawn);
            if(validTargets.EnumerableNullOrEmpty())
            {
                RV2Log.Message("Early exit: no targets at all", true, false, "AutoVore");
                return null;
            }
#if !v1_2
            if(ModLister.IdeologyInstalled)
            {
                if(TryAttemptIdeologyJobInterception(pawn, validTargets, out VoreJob ideologyJob))
                {
                    return ideologyJob;
                }
            }
#endif
            Pawn target = RollForTarget(pawn, validTargets);
            if(target == null)
            {
                RV2Log.Message("Early exit: no target determinable", true, false, "AutoVore");
                return null;
            }

            VoreInteractionRequest directRequest = new VoreInteractionRequest(pawn, target, VoreRole.Invalid, true);
            VoreInteraction interaction = VoreInteractionManager.Retrieve(directRequest);
            if(interaction.PreferredPath == null)
            {
                RV2Log.Message("Early exit: no preferred path", true, false, "AutoVore");
                return null;
            }
            VorePathDef pathDef = interaction.PreferredPath;
            RV2Log.Message("Picked path for vore proposal: " + pathDef.defName, true, false, "AutoVore");
            return CreateJob(interaction.Predator, interaction.Prey, pawn, target, pathDef);
        }
#if !v1_2

        private bool TryAttemptIdeologyJobInterception(Pawn pawn, IEnumerable<Pawn> targets, out VoreJob job)
        {
            job = null;
            Ideo ideo = pawn.Ideo;
            if(ideo == null)
            {
                RV2Log.Message("Pawn has no ideo, skipping ideology intercept", true, false, "AutoVore");
                return false;
            }
            Pawn alphaPredator = targets.FirstOrDefault(t => ideo.GetRole(t)?.def == RV2_Common.IdeoRolePredator);
            Pawn chosenPrey = targets.FirstOrDefault(t => ideo.GetRole(t)?.def == RV2_Common.IdeoRolePrey);
            if(alphaPredator != null && !alphaPredator.CanVore(pawn, out string reason))
            {
                RV2Log.Message("Alpha predator found, but they can not vore the current pawn, reason: " + reason, true, false, "AutoVore");
                alphaPredator = null;
            }
            if(chosenPrey != null && !pawn.CanVore(chosenPrey, out reason))
            {
                RV2Log.Message("Chosen prey found, but they can not be vored by the current pawn, reason: " + reason, true, false, "AutoVore");
                chosenPrey = null;
            }
            if(alphaPredator == null && chosenPrey == null)
            {
                RV2Log.Message("No ideology role target, skipping ideology intercept", true, false, "AutoVore");
                return false;
            }
            if(!Rand.Chance(RV2Mod.Settings.ideology.AutoVoreInterceptionChance))
            {
                RV2Log.Message("Random chance for ideology intercept failed, chance was " + RV2Mod.Settings.ideology.AutoVoreInterceptionChance + "%", true, false, "AutoVore");
                return false;
            }
            VoreRole forcedRole;
            Pawn target;
            if(alphaPredator != null && chosenPrey != null)
            {
                if(Rand.Chance(0.5f))
                {
                    RV2Log.Message("Chosen Prey and Alpha Predator available, picked alpha predator", true, false, "AutoVore");
                    target = alphaPredator;
                    forcedRole = VoreRole.Prey;
                }
                else
                {
                    RV2Log.Message("Chosen Prey and Alpha Predator available, picked chosen prey", true, false, "AutoVore");
                    target = chosenPrey;
                    forcedRole = VoreRole.Predator;
                }
            }
            else if(alphaPredator != null)
            {
                RV2Log.Message("Alpha Predator available and picked", true, false, "AutoVore");
                target = alphaPredator;
                forcedRole = VoreRole.Prey;
            }
            else
            {
                RV2Log.Message("Chosen Prey available and picked", true, false, "AutoVore");
                target = chosenPrey;
                forcedRole = VoreRole.Predator;
            }
            RV2Log.Message("Target: " + target.LabelShort + " initiator role: " + forcedRole, true, false, "AutoVore");
            VoreInteractionRequest request = new VoreInteractionRequest(pawn, target, forcedRole, true);
            VoreInteraction interaction = VoreInteractionManager.Retrieve(request);
            if(interaction.PreferredPath == null)
            {
                RV2Log.Message("No path available", true, false, "AutoVore");
                return false;
            }
            VorePathDef pathDef = interaction.PreferredPath;
            RV2Log.Message("Picked path for vore proposal: " + pathDef.defName, true, false, "AutoVore");
            job = CreateJob(interaction.Predator, interaction.Prey, pawn, target, pathDef);
            return true;
        }
#endif

        protected virtual VoreJob CreateJob(Pawn predator, Pawn prey, Pawn initiator, Pawn target, VorePathDef path)
        {
            VoreProposal_TwoWay proposal = new VoreProposal_TwoWay(predator, prey, initiator, target, path);
            VoreJob job = VoreJobMaker.MakeJob(VoreJobDefOf.RV2_ProposeVore, initiator, target);
            job.targetA = target;
            job.Proposal = proposal;
            job.VorePath = proposal.VorePath;
            return job;
        }

        private Pawn RollForTarget(Pawn pawn, IEnumerable<Pawn> targets)
        {
            Dictionary<Pawn, float> weightedTargets;
            if(!TryMakeWeightedPreferences(pawn, targets, out weightedTargets))
            {
                return null;
            }
            RV2Log.Message("weighted targets: " + LogUtility.ToString(weightedTargets), true, false, "AutoVore");

            Pawn target = weightedTargets.RandomElementByWeight(kvp => kvp.Value).Key;
            RV2Log.Message("Picked target " + target.LabelShort, true, false, "AutoVore");
            return target;
        }

        private bool TryMakeWeightedPreferences(Pawn pawn, IEnumerable<Pawn> targets, out Dictionary<Pawn, float> weightedPreferences)
        {
            weightedPreferences = new Dictionary<Pawn, float>();
            if(targets.EnumerableNullOrEmpty())
            {
                return false;
            }
            foreach(Pawn target in targets)
            {
                float preference = pawn.PreferenceFor(target);
                if(preference >= 0)
                {
                    //preference++;
                    weightedPreferences.Add(target, preference);
                }
                else
                {
                    RV2Log.Message("Preference for target is below 0, not adding to valid target list", true, false, "AutoVore");
                }
            }
            return !weightedPreferences.EnumerableNullOrEmpty();
        }
    }
}
